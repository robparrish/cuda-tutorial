#ifndef TRANSPOSE_FILL_H
#define TRANSPOSE_FILL_H

template<typename T>
void fill(
    T* data_d,
    size_t size);
    
template<typename T>
void fill_scalar(
    T* data_d,
    size_t size)
{
    T* data_h = (T*) malloc(size*sizeof(T));
    for (size_t ind = 0; ind < size; ind++) {
        data_h[ind] = (T) ind;
    }    
    cudaMemcpy(data_d,data_h,size*sizeof(T),cudaMemcpyHostToDevice);

    free(data_h);
}

template<>
void fill<char>(
    char* data_d,
    size_t size)
{
    fill_scalar(data_d,size);
}

template<>
void fill<double>(
    double* data_d,
    size_t size)
{
    fill_scalar(data_d,size);
}

template<>
void fill<float>(
    float* data_d,
    size_t size)
{
    fill_scalar(data_d,size);
}

template<typename T>
void fill_vector(
    T* data_d,
    size_t size)
{
    T* data_h = (T*) malloc(size*sizeof(T));
    for (size_t ind = 0; ind < size; ind++) {
        data_h[ind].x = ind;
    }    
    cudaMemcpy(data_d,data_h,size*sizeof(T),cudaMemcpyHostToDevice);

    free(data_h);
}

template<>
void fill<float2>(
    float2* data_d,
    size_t size)
{
    fill_vector(data_d,size);
}

template<>
void fill<float3>(
    float3* data_d,
    size_t size)
{
    fill_vector(data_d,size);
}

template<>
void fill<float4>(
    float4* data_d,
    size_t size)
{
    fill_vector(data_d,size);
}

template<>
void fill<double2>(
    double2* data_d,
    size_t size)
{
    fill_vector(data_d,size);
}

template<>
void fill<double3>(
    double3* data_d,
    size_t size)
{
    fill_vector(data_d,size);
}

template<>
void fill<double4>(
    double4* data_d,
    size_t size)
{
    fill_vector(data_d,size);
}

#endif
