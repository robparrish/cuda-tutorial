#include <cstdio>    
#include <stdexcept>

#include "fill.h"
#include "check.h"

// => CUDA Error Checking Macro <= //

/**
 * Place a "CUERR;" statement after each CUDA API call to check CUDA error
 * state during debugging
 **/
#define CUERR { \
 cudaError_t e=cudaGetLastError(); \
 if(e!=cudaSuccess) { \
   printf("Cuda failure %s:%d: '%s'\n",__FILE__,__LINE__,cudaGetErrorString(e)); \
   exit(0); \
 } \
}

template <typename T>
__global__ void copy_naive(
    const T* A,
    T* B)
{
    int i = blockDim.x * blockIdx.x + threadIdx.x;
    int j = blockDim.y * blockIdx.y + threadIdx.y;
    int ni = blockDim.x * gridDim.x;
    int ind1 = i + ni * j;

    B[ind1] = A[ind1];
}

template <typename T>
__global__ void trans_naive(
    const T* A,
    T* B)
{
    int i = blockDim.x * blockIdx.x + threadIdx.x;
    int j = blockDim.y * blockIdx.y + threadIdx.y;
    int ni = blockDim.x * gridDim.x;
    int nj = blockDim.y * gridDim.y;
    int ind1 = i + ni * j;
    int ind2 = j + nj * i;

    B[ind2] = A[ind1];
}

template <typename T>
__global__ void trans_shared(
    const T* A,
    T* B,
    int PAD)
{
    extern __shared__ float S1T[];
    T* S1 = (T*) S1T;
    
    int i = blockDim.x * blockIdx.x + threadIdx.x;
    int j = blockDim.y * blockIdx.y + threadIdx.y;

    int ni = blockDim.x * gridDim.x;
    int ind1 = i + ni * j;
    int sind1 = threadIdx.x + (blockDim.x + PAD) * threadIdx.y;
    S1[sind1] = A[ind1];

    __syncthreads();

    int nj = blockDim.y * gridDim.y;
    int i2 = blockDim.y * blockIdx.x + threadIdx.x;
    int j2 = blockDim.x * blockIdx.y + threadIdx.y;
    int ind2 = i2 + nj * j2;
    int sind2 = threadIdx.y + (blockDim.x + PAD) * threadIdx.x;

    B[ind2] = S1[sind2];
}

template <typename T>
void test(int size, int block_size)
{
    dim3 blockDim(block_size,block_size);
    dim3 gridDim(size / block_size, size / block_size);
    float t_ms;

    T* d1;
    T* d2;
    cudaMalloc(&d1, size*size*sizeof(T)); CUERR;
    cudaMalloc(&d2, size*size*sizeof(T)); CUERR;

    cudaEvent_t start, stop;

    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    cudaStream_t stream;
    cudaStreamCreate(&stream);
    
    fill<T>(d1,size);
    copy_naive<T><<<gridDim,blockDim,0,stream>>>(d1, d2); // Warm-up
    cudaEventRecord(start,stream);
    copy_naive<T><<<gridDim,blockDim,0,stream>>>(d2, d1);
    cudaEventRecord(stop,stream);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&t_ms,start,stop);
    printf("Naive Copy:           %11.3E [ms] %11.3E [GB/s]\n", t_ms, 2*size*size*sizeof(T) / (1024*1024*t_ms));
    check<T>(d1,size);
    
    fill<T>(d1,size);
    trans_naive<T><<<gridDim,blockDim,0,stream>>>(d1, d2); // Warm-up
    cudaEventRecord(start,stream);
    trans_naive<T><<<gridDim,blockDim,0,stream>>>(d2, d1);
    cudaEventRecord(stop,stream);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&t_ms,start,stop);
    printf("Naive Trans:          %11.3E [ms] %11.3E [GB/s]\n", t_ms, 2*size*size*sizeof(T) / (1024*1024*t_ms));
    check<T>(d1,size);
    
    fill<T>(d1,size);
    trans_shared<T><<<gridDim,blockDim,block_size*(block_size+0)*sizeof(T),stream>>>(d1, d2, 0); // Warm-up
    cudaEventRecord(start,stream);
    trans_shared<T><<<gridDim,blockDim,block_size*(block_size+0)*sizeof(T),stream>>>(d2, d1, 0); 
    cudaEventRecord(stop,stream);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&t_ms,start,stop);
    printf("Shared Trans (Pad=0): %11.3E [ms] %11.3E [GB/s]\n", t_ms, 2*size*size*sizeof(T) / (1024*1024*t_ms));
    check<T>(d1,size);
    
    fill<T>(d1,size);
    trans_shared<T><<<gridDim,blockDim,block_size*(block_size+1)*sizeof(T),stream>>>(d1, d2, 1); // Warm-up
    cudaEventRecord(start,stream);
    trans_shared<T><<<gridDim,blockDim,block_size*(block_size+1)*sizeof(T),stream>>>(d2, d1, 1); 
    cudaEventRecord(stop,stream);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&t_ms,start,stop);
    printf("Shared Trans (Pad=1): %11.3E [ms] %11.3E [GB/s]\n", t_ms, 2*size*size*sizeof(T) / (1024*1024*t_ms));
    check<T>(d1,size);
    
    cudaEventDestroy(start);
    cudaEventDestroy(stop);

    cudaStreamDestroy(stream);
    
    cudaFree(d1); CUERR;
    cudaFree(d2); CUERR;
}

int main()
{
    printf("char\n");
    test<char>(1024,32);    
    printf("float\n");
    test<float>(1024,32);    
    printf("double\n");
    test<double>(1024,32);    
    printf("float2\n");
    test<float2>(1024,32);    
    printf("double2\n");
    test<double2>(1024,32);    
    printf("float3\n");
    test<float3>(1024,32);    
    printf("double3\n");
    test<double3>(1024,32);    
    printf("float4\n");
    test<float4>(1024,32);    
    printf("double4\n");
    test<double4>(1024,32);    
}
