import numpy as np
import matplotlib.pyplot as plt

julia = np.loadtxt('julia.dat')
# Makes a "jet" colormap with 1 million colors
cmap = plt.cm.get_cmap('jet',1E6)
norm = plt.Normalize(vmin=julia.min(),vmax=julia.max())
image = cmap(norm(julia)) 
plt.imsave('julia.png', image)
