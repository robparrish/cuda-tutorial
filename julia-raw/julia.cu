#include <cstdio>
#include <stdexcept>

// => CUDA Error Checking Macro <= //

/**
 * Place a "CUERR;" statement after each CUDA API call to check CUDA error
 * state during debugging
 **/
#define CUERR { \
 cudaError_t e=cudaGetLastError(); \
 if(e!=cudaSuccess) { \
   printf("Cuda failure %s:%d: '%s'\n",__FILE__,__LINE__,cudaGetErrorString(e)); \
   exit(0); \
 } \
}

// => Main Program (Runs on Host) <= //

int main(int argc, const char** argv)
{
    // Set to use the assigned device
    cudaSetDevice(0); CUERR;

    // Parameters (e.g., read from stdin or passed from python or read from an input file).
    if (argc != 15) throw std::runtime_error("Usage: ./julia dimx dimy subx suby ox oy sx sy cx cy maxiter escape blockx blocky");
    int2 dim = { atoi(argv[1]), atoi(argv[2]) };
    int2 sub = { atoi(argv[3]), atoi(argv[4]) };
    float2 o = { atof(argv[5]), atof(argv[6]) };
    float2 s = { atof(argv[7]), atof(argv[8]) };
    float2 c = { atof(argv[9]), atof(argv[10]) };
    int maxiter = atoi(argv[11]);
    float escape = atof(argv[12]);
    dim3 block_size(atoi(argv[13]),atoi(argv[14]));
    
    // The image, striped as y, x
    float* image_h = (float*) malloc(dim.y*dim.x*sizeof(float));

    // TODO: You need to fill image_h with your Julia image
    
    // I/O: save the array to image.dat for the user
    FILE* fh = fopen("julia.dat","w");
    for (int i = 0; i < dim.y; i++) {
        for (int j = 0; j < dim.x; j++) {
            fprintf(fh, "%16.8E ", image_h[i*dim.x + j]);
        }
        fprintf(fh,"\n");
    }
    fclose(fh);

    // Always free your memory
    free(image_h);

    return 0;
}
