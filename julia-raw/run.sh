#Usage ./julia dimx dimy subx suby ox oy sx sy cx cy maxiter escape blockx blocky

# Classic
./julia 2048 1024 40 40 -2.0 -1.0 4.0 2.0 -0.80 -0.156 200 2000.0 8 8

# Cyclones
#./julia 2048 1024 40 40 -2.0 -1.0 4.0 2.0 -0.7269 0.1889 200 2000.0 8 8

# Lightning
#./julia 2048 1024 40 40 -2.0 -1.0 4.0 2.0 0.0 -0.8 200 2000.0 8 8

# Snowflakes
#./julia 2048 1024 40 40 -2.0 -1.0 4.0 2.0 -0.70176 -0.3842 200 2000.0 8 8

# Flowers
#./julia 2048 1024 40 40 -2.0 -1.0 4.0 2.0 -0.4 0.6 200 2000.0 8 8

# Dragon
#./julia 2048 1024 40 40 -2.0 -1.0 4.0 2.0 -0.835 -0.2321 200 2000.0 8 8

# Islands
#./julia 2048 1024 40 40 -2.0 -1.0 4.0 2.0 0.285 -0.01 200 2000.0 8 8

#./julia 2048 1024 40 40 0.2 0.13 0.0025 0.00125 -0.79 0.15 200 2000.0 8 8

# Make julia.png from julia.dat
python julia.py
