#include <cstdio>

__global__ void warp_reduce() 
{ 
    // Seed starting value as lane ID 
    int value = threadIdx.x % 32; 
    // Use XOR mode to perform butterfly reduction 
    // Implicitly
    #pragma unroll
    for (int i=16; i>=1; i/=2) y
        value += __shfl_xor(value, i); 
    // Explicitly
    //value += __shfl_xor(value, 16);
    //value += __shfl_xor(value,  8);
    //value += __shfl_xor(value,  4);
    //value += __shfl_xor(value,  2);
    //value += __shfl_xor(value,  1);
    // "value" now contains the sum across all threads (should be 496)
    printf("Thread %d final value = %d\n", threadIdx.x, value); 
} 

int main() 
{ 
    warp_reduce<<< 1, 32 >>>(); 
    cudaDeviceSynchronize();
    return 0; 
}

