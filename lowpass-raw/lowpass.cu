#include <cstdio>    
#include <stdexcept>

// => CUDA Error Checking Macro <= //

/**
 * Place a "CUERR;" statement after each CUDA API call to check CUDA error
 * state during debugging
 **/
#define CUERR { \
 cudaError_t e=cudaGetLastError(); \
 if(e!=cudaSuccess) { \
   printf("Cuda failure %s:%d: '%s'\n",__FILE__,__LINE__,cudaGetErrorString(e)); \
   exit(0); \
 } \
}

// => Moving Average Kernel <= //

// TODO

// => Main Program <= //

int main()
{
    // Sizes determined at runtime
    int R = 7; // Radius of average (average will use 2*R+1 points)
    int nblock = 10; // number of blocks in grid (gridDim.x)
    int nthread = 512; // number of threads in block (blockDim.x)
    int nvec = nblock*nthread; // number of elements in signal

    // Host-side arrays
    float* raw_h = (float*) malloc(sizeof(float)*nvec);
    float* fin_h = (float*) malloc(sizeof(float)*nvec);
    // A noise sinusoid
    for (int index = 0; index < nvec; index++) {
        float t = index / ((float) nvec);
        raw_h[index] = sin(2.0f*M_PI*t) + (0.1f * rand()) / RAND_MAX;
    }

    // Output finished signal to lowpass.dat
    FILE* fh = fopen("lowpass.dat", "w");
    for (int index = 0; index < nvec; index++) {
        float t = index / ((float) nvec);
        fprintf(fh,"%14.7f %14.7f %14.7f\n", t, raw_h[index], fin_h[index]);
    }
    fclose(fh);

    // Always free memory
    free(raw_h);
    free(fin_h);
}

