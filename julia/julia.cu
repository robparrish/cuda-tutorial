#include <cstdio>    
#include <stdexcept>

// => CUDA Error Checking Macro <= //

/**
 * Place a "CUERR;" statement after each CUDA API call to check CUDA error
 * state during debugging
 **/
#define CUERR { \
 cudaError_t e=cudaGetLastError(); \
 if(e!=cudaSuccess) { \
   printf("Cuda failure %s:%d: '%s'\n",__FILE__,__LINE__,cudaGetErrorString(e)); \
   exit(0); \
 } \
}

// => Complex Math Library (For Device) <= //
    
/**
 * Compute |x|^2 where x is a complex number represented as float2
 * @param x the complex number whose norm2 is to be computed
 * @return the square of the norm
 **/
__device__ __forceinline__
float norm2(float2 x)
{
    return x.x * x.x + x.y * x.y;
}
/**
 * Compute z = x + y where x, y, and z are complex numbers represented as float2
 * @param x one of the addends
 * @param y one of the addends
 * @return z the sum
 **/
__device__ __forceinline__
float2 add(float2 x, float2 y) {
    float2 z;
    z.x = x.x + y.x;
    z.y = x.y + y.y;
    return z;
}
/**
 * Compute z = x * y where x, y, and z are complex numbers represented as float2
 * @param x one of the factors
 * @param y one of the factors
 * @return z the product
 **/
__device__ __forceinline__ 
float2 mult(float2 x, float2 y) { 
    float2 z;
    z.x = x.x * y.x - x.y * y.y;
    z.y = x.x * y.y + x.y * y.x;
    return z;
}

// => CUDA Implementation of Julia Set Sampling <= //

/**
 * Compute the Julia fractal fractional escape time at a given point
 * @param x the coordinates of the point
 * @param c the c argument defining the Julia fractal
 * @param maxiter the maximum number of iterations to run
 * @param escape the square of the norm to declare an escape
 * @return the fractional escape time
 **/
__device__ __forceinline__
float julia(float2 x, float2 c, int maxiter, float escape)
{
    int i = 0;
    for (;i < maxiter;) {
        // Perform the Julia iteration
        x = add(mult(x,x),c);
        i++;
        // If escaped, break
        if (norm2(x) > escape) break;
    }
    if (i == maxiter) return i; // prevents numerical problems for |z| << 1
    x = add(mult(x,x),c); i++; // Two more iterations help provide higher accuracy
    x = add(mult(x,x),c); i++;
    // Fractional escape time (from the online literature)
    return i + 2 - log(log(norm2(x))) / 0.69314718056f;
}

/**
 * Compute the block contribution to the Julia fractal 
 * @param image a dim.y*dim.x float array to place the fractional escape time
 *  scalar field into. x is fast, y is slow
 * @param sub the number of subsamples in x and y
 * @param o the image origin (lower-left corner)
 * @param s the image scale (width and height)
 * @param c the c argument defining the Julia fractal
 * @param maxiter the maximum number of iterations to run
 * @param escape the square of the norm to declare an escape
 * @result the image values for this block's tasking are overwritten with the
 *  average of the fractional escape time over the subsampling scheme.
 **/
__global__ void julia_kernel(
    float* image,
    int2 sub,
    float2 o,
    float2 s,
    float2 c,
    int maxiter,
    float escape)
{
    // Determine the pixel index for the current thread
    int2 ind = {
        threadIdx.x + blockDim.x * blockIdx.x,    
        threadIdx.y + blockDim.y * blockIdx.y};    
    // Determine the image dimensions in pixels (inferred from grid/block dims)
    int2 dim = {
        blockDim.x * gridDim.x,
        blockDim.y * gridDim.y};
    // Determine the center of the current thread
    float2 pos = {
        s.x * (((ind.x + 0.5f) / (float) dim.x)) + o.x,
        s.y * (((ind.y + 0.5f) / (float) dim.y)) + o.y};
    // Determine the subsampling widths
    float2 d = {
        s.x / (dim.x * sub.x),
        s.y / (dim.y * sub.y)};
    // Peform the subsampling
    float jval = 0.0;
    for (int i = 0; i < sub.x; i++) {
        for (int j = 0; j < sub.y; j++) {
            float2 pos2 = {
                pos.x + (i - 0.5f*(sub.x-1)) * d.x, 
                pos.y + (j - 0.5f*(sub.y-1)) * d.y };
            jval += julia(pos2,c,maxiter,escape);
        }
    }
    // Write the result to global memory
    image[ind.x + dim.x*ind.y] = jval / (float) (sub.x * sub.y); 
}    

// => Main Program (Runs on Host) <= //

int main(int argc, const char** argv)
{
    // Set to use the assigned device
    cudaSetDevice(0); CUERR;

    // Parameters (e.g., read from stdin or passed from python or read from an input file).
    if (argc != 15) throw std::runtime_error("Usage: ./julia dimx dimy subx suby ox oy sx sy cx cy maxiter escape blockx blocky");
    int2 dim = { atoi(argv[1]), atoi(argv[2]) };
    int2 sub = { atoi(argv[3]), atoi(argv[4]) };
    float2 o = { atof(argv[5]), atof(argv[6]) };
    float2 s = { atof(argv[7]), atof(argv[8]) };
    float2 c = { atof(argv[9]), atof(argv[10]) };
    int maxiter = atoi(argv[11]);
    float escape = atof(argv[12]);
    dim3 block_size(atoi(argv[13]),atoi(argv[14]));
    
    // Set up the grid/block dimensions
    dim3 grid_size(dim.x / block_size.x, dim.y / block_size.y);
    if (grid_size.x * block_size.x != dim.x || grid_size.y * block_size.y != dim.y) {   
        throw std::runtime_error("Invalid dim/block_size parameters");
    }

    // Target: a float* containing the image on the host
    float* image_h = (float*) malloc(dim.y*dim.x*sizeof(float));

    // Intermediate: a float* to compute the image on the device
    float* image_d;
    cudaMalloc(&image_d, dim.y*dim.x*sizeof(float)); CUERR;
    cudaMemset(image_d,0,dim.y*dim.x*sizeof(float)); CUERR;

    // Time the Julia kernel
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);
    cudaEventRecord(start);

    // GPU Julia kernel
    julia_kernel<<<grid_size,block_size>>>(image_d,sub,o,s,c,maxiter,escape); CUERR;

    // Evaluate the timing of the Julia kernel
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    float t_ms;
    cudaEventElapsedTime(&t_ms,start,stop);
    cudaEventDestroy(start);
    cudaEventDestroy(stop);
    printf("Elapsed time: %11.3E [ms]\n", t_ms);

    // Device-to-Host data transfer
    cudaMemcpy(image_h,image_d,dim.y*dim.x*sizeof(float),cudaMemcpyDeviceToHost); CUERR;
    cudaFree(image_d); CUERR;

    // I/O: save the array to image.dat for the user
    FILE* fh = fopen("julia.dat","w");
    for (int i = 0; i < dim.y; i++) {
        for (int j = 0; j < dim.x; j++) {
            fprintf(fh, "%16.8E ", image_h[i*dim.x + j]);
        }
        fprintf(fh,"\n");
    }
    fclose(fh);

    // Always free your memory
    free(image_h);

    return 0;
}
