#!/usr/bin/env python
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

lowpass = np.loadtxt('lowpass.dat')

plt.clf()
plt.plot(lowpass[:,0],lowpass[:,1])
plt.plot(lowpass[:,0],lowpass[:,2])
plt.xlabel('x')
plt.ylabel('y')
plt.legend(['raw', 'fin'])
plt.savefig('lowpass.pdf')
