#include <cstdio>    
#include <stdexcept>

// => CUDA Error Checking Macro <= //

/**
 * Place a "CUERR;" statement after each CUDA API call to check CUDA error
 * state during debugging
 **/
#define CUERR { \
 cudaError_t e=cudaGetLastError(); \
 if(e!=cudaSuccess) { \
   printf("Cuda failure %s:%d: '%s'\n",__FILE__,__LINE__,cudaGetErrorString(e)); \
   exit(0); \
 } \
}

// => Moving Average Kernel <= //

/**
 * Compute the symmetric moving average of radius R for a signal in raw,
 * placing the result in fin, using a 1D grid of 1D blocks. This kernel
 * requires sizeof(float)*(blockDim.x + 2*R) shared memory.
 *
 * @param raw the input signal, a blockDim.x*gridDim.x array
 * @param fin the output signal, a blockDim.x*gridDim.x array
 * @param R the moving average radius
 **/
__global__ void lowpass(
    const float* raw,
    float* fin,
    int R)
{
    // The shared memory cache
    extern __shared__ float cache[];

    // The absolute index of the point assigned to this thread
    int index = threadIdx.x + blockDim.x * blockIdx.x; 

    // Load the raw signal into cache, including halos
    cache[threadIdx.x + R] = raw[index]; // Core signal
    if (threadIdx.x < R) {
        cache[threadIdx.x] = (index - R >= 0 ? raw[index - R] : 0.0f); // Left Halo
        cache[threadIdx.x + R + blockDim.x] = (index + blockDim.x < blockDim.x*gridDim.x ? raw[index + blockDim.x] : 0.0f); // Right Halo
    }

    // Make sure all threads complete loads before using cache
    __syncthreads();

    // Perform moving average for this pixel
    float temp = 0.0f;
    for (int index2 = -R; index2 <= R; index2++) {
        temp += cache[threadIdx.x + index2 + R];
    }
    temp /= (2 * R + 1); 

    // Output 
    fin[index] = temp;
}

// => Main Program <= //

int main()
{
    // Sizes determined at runtime
    int R = 7; // Radius of average (average will use 2*R+1 points)
    int nblock = 10; // number of blocks in grid (gridDim.x)
    int nthread = 512; // number of threads in block (blockDim.x)
    int nvec = nblock*nthread; // number of elements in signal

    // Host-side arrays
    float* raw_h = (float*) malloc(sizeof(float)*nvec);
    float* fin_h = (float*) malloc(sizeof(float)*nvec);
    // A noise sinusoid
    for (int index = 0; index < nvec; index++) {
        float t = index / ((float) nvec);
        raw_h[index] = sin(2.0f*M_PI*t) + (0.1f * rand()) / RAND_MAX;
    }

    // Device-side arrays
    float* raw_d;
    float* fin_d;
    cudaMalloc(&raw_d,sizeof(float)*nvec); CUERR;
    cudaMalloc(&fin_d,sizeof(float)*nvec); CUERR;

    // Copy noisy signal to device
    cudaMemcpy(raw_d,raw_h,sizeof(float)*nvec,cudaMemcpyHostToDevice); CUERR;

    // Lowpass filter the signal on the device
    lowpass<<<nblock,nthread,sizeof(float)*(nthread+2*R)>>>(raw_d,fin_d,R); CUERR;
    
    // Copy finished signal to host
    cudaMemcpy(fin_h,fin_d,sizeof(float)*nvec,cudaMemcpyDeviceToHost); CUERR;

    // Output finished signal to lowpass.dat
    FILE* fh = fopen("lowpass.dat", "w");
    for (int index = 0; index < nvec; index++) {
        float t = index / ((float) nvec);
        fprintf(fh,"%14.7f %14.7f %14.7f\n", t, raw_h[index], fin_h[index]);
    }
    fclose(fh);

    // Always free memory
    cudaFree(raw_d);
    cudaFree(fin_d);
    free(raw_h);
    free(fin_h);
}

