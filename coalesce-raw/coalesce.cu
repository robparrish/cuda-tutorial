#include <cstdio>    
#include <stdexcept>

// => CUDA Error Checking Macro <= //

/**
 * Place a "CUERR;" statement after each CUDA API call to check CUDA error
 * state during debugging
 **/
#define CUERR { \
 cudaError_t e=cudaGetLastError(); \
 if(e!=cudaSuccess) { \
   printf("Cuda failure %s:%d: '%s'\n",__FILE__,__LINE__,cudaGetErrorString(e)); \
   exit(0); \
 } \
}

