#!/usr/bin/env python
import re
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

def parse(
    filename,
    keys=[
        'char', 
        'float',
        'float2',
        'float3',
        'float4',
        'double',
        'double2',
        'double3',
        'double4',
        ],
    ):
    
    lines = open(filename).readlines()

    starts = { key : [lind for lind, line in enumerate(lines) if re.match(r'^%s\n' % (key), line)][0] for key in keys }
    stops = { key : min([x for x in  starts.values() + [len(lines)] if x > val]) for key, val in starts.iteritems() }

    strides = {}
    offsets = {}
    for key in keys:
        lines2 = lines[starts[key]+2:stops[key]]
        offset_lines = lines2[:33]
        stride_lines = lines2[34:]
        offsets[key] = np.array([float(re.match(r'^\s*\d+\s*:\s+(\S+)\s*$', line).group(1)) for line in offset_lines])
        strides[key] = np.array([float(re.match(r'^\s*\d+\s*:\s+(\S+)\s*$', line).group(1)) for line in stride_lines])

    return strides, offsets
        
def plot_strides(
    filename,
    strides,
    N=np.arange(1,33),
    keys=[
        'char', 
        'float',
        'float2',
        'float3',
        'float4',
        'double',
        'double2',
        'double3',
        'double4',
        ],
    styles={
        'char' : '-g',
        'float' : '-b',
        'float2' : '-ob',
        'float3' : '-vb',
        'float4' : '-sb',
        'double' : '-r',
        'double2' : '-or',
        'double3' : '-vr',
        'double4' : '-sr',
        },
    ):        

    plt.clf()
    for key in keys:
        val = strides[key]
        plt.plot(N,val,styles[key],label=key)
    plt.xlabel('Stride [Elements]')
    plt.ylabel('Bandwidth [GB/s]')
    plt.legend(loc=1)
    plt.savefig(filename)

def plot_offsets(
    filename,
    offsets,
    N=np.arange(0,33),
    keys=[
        'char', 
        'float',
        'float2',
        'float3',
        'float4',
        'double',
        'double2',
        'double3',
        'double4',
        ],
    styles={
        'char' : '-g',
        'float' : '-b',
        'float2' : '-ob',
        'float3' : '-vb',
        'float4' : '-sb',
        'double' : '-r',
        'double2' : '-or',
        'double3' : '-vr',
        'double4' : '-sr',
        },
    ):        

    plt.clf()
    for key in keys:
        val = offsets[key]
        plt.plot(N,val,styles[key],label=key)
    plt.xlabel('Offset [Elements]')
    plt.ylabel('Bandwidth [GB/s]')
    plt.legend(loc=1)
    plt.savefig(filename)
    
# 192 GB/s
#strides, offsets = parse('970.dat')
#plot_strides('970_strides.pdf',strides)
#plot_offsets('970_offsets.pdf',offsets)
    
# 320 GB/s
#strides, offsets = parse('1080.dat')
#plot_strides('1080_strides.pdf',strides)
#plot_offsets('1080_offsets.pdf',offsets)

# 336 GB/s
#strides, offsets = parse('titan_x.dat')
#plot_strides('titan_x_strides.pdf',strides)
#plot_offsets('titan_x_offsets.pdf',offsets)

# 420 GB/s
#strides, offsets = parse('titan_x_pascal.dat')
#plot_strides('titan_x_pascal_strides.pdf',strides)
#plot_offsets('titan_x_pascal_offsets.pdf',offsets)
    
