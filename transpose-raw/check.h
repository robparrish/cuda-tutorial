#ifndef TRANSPOSE_CHECK_H
#define TRANSPOSE_CHECK_H

#include <stdexcept>

template<typename T>
void check(
    T* data_d,
    size_t size);
    
template<typename T>
void check_scalar(
    T* data_d,
    size_t size)
{
    T* data_h = (T*) malloc(size*sizeof(T));
    cudaMemcpy(data_h,data_d,size*sizeof(T),cudaMemcpyDeviceToHost);
    for (size_t ind = 0; ind < size; ind++) {
        T val = (T) ind;
        if (data_h[ind] != val) throw std::runtime_error("Bad check");
    }    
    free(data_h);
}

template<>
void check<char>(
    char* data_d,
    size_t size)
{
    check_scalar(data_d,size);
}

template<>
void check<double>(
    double* data_d,
    size_t size)
{
    check_scalar(data_d,size);
}

template<>
void check<float>(
    float* data_d,
    size_t size)
{
    check_scalar(data_d,size);
}

template<typename T>
void check_vector(
    T* data_d,
    size_t size)
{
    T* data_h = (T*) malloc(size*sizeof(T));
    cudaMemcpy(data_h,data_d,size*sizeof(T),cudaMemcpyDeviceToHost);
    for (size_t ind = 0; ind < size; ind++) {
        T val;
        val.x = ind;
        if (data_h[ind].x != val.x) throw std::runtime_error("Bad check");
        
    }    
    free(data_h);
}

template<>
void check<float2>(
    float2* data_d,
    size_t size)
{
    check_vector(data_d,size);
}

template<>
void check<float3>(
    float3* data_d,
    size_t size)
{
    check_vector(data_d,size);
}

template<>
void check<float4>(
    float4* data_d,
    size_t size)
{
    check_vector(data_d,size);
}

template<>
void check<double2>(
    double2* data_d,
    size_t size)
{
    check_vector(data_d,size);
}

template<>
void check<double3>(
    double3* data_d,
    size_t size)
{
    check_vector(data_d,size);
}

template<>
void check<double4>(
    double4* data_d,
    size_t size)
{
    check_vector(data_d,size);
}

#endif
