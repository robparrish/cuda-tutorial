#!/usr/bin/env python
import re
import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

def parse(
    filename,
    keys = [
        'H2H',
        'H2D Unpinned',
        'H2D Pinned',
        'D2H Unpinned',
        'D2H Pinned',
        'D2D'],
    ):
    
    lines = open(filename).readlines()

    sizes = [int(re.match(r'Size:\s+(\d+)',x).group(1)) for x in lines if re.match(r'Size:',x)]


    speeds = {}
    for key in keys:
        vals = []
        for line in lines:
            mobj = re.match(r'%s:\s+Time =\s+\S+ \[ms\], Bandwidth =\s+(\S+)' % key, line)
            if mobj:
                vals.append(float(mobj.group(1)))
        speeds[key] = np.array(vals)
                
    return sizes, speeds
        
def plot_transfers(
    filename,
    sizes,
    speeds,
    keys = [
        'H2H',
        'H2D Unpinned',
        'H2D Pinned',
        'D2H Unpinned',
        'D2H Pinned',
        'D2D'],
    styles={
        'H2H': '-ro',
        'H2D Unpinned': '-go',
        'H2D Pinned': '--gv',
        'D2H Unpinned': '-bo',
        'D2H Pinned': '--bv',
        'D2D': '-ko',
        },
    ):        

    plt.clf()
    for key in keys:
        val = speeds[key]
        plt.semilogx(sizes,val,styles[key],label=key)
    plt.xlabel('Transfer Size [Bytes]')
    plt.ylabel('Effective Bandwidth [GB/s]')
    plt.legend(loc=2)
    plt.savefig(filename)

# 192 GB/s
sizes, speeds = parse('970.dat')
plot_transfers('970.pdf', sizes, speeds)
    
# 320 GB/s
sizes, speeds = parse('1080.dat')
plot_transfers('1080.pdf', sizes, speeds)

# 420 GB/s
sizes, speeds = parse('titan_x_pascal.dat')
plot_transfers('titan_x_pascal.pdf', sizes, speeds)
    
