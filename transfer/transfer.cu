#include <cstdio>    
#include <stdexcept>

// => CUDA Error Checking Macro <= //

/**
 * Place a "CUERR;" statement after each CUDA API call to check CUDA error
 * state during debugging
 **/
#define CUERR { \
 cudaError_t e=cudaGetLastError(); \
 if(e!=cudaSuccess) { \
   printf("Cuda failure %s:%d: '%s'\n",__FILE__,__LINE__,cudaGetErrorString(e)); \
   exit(0); \
 } \
}

// => Memcpy Test Code <= //

/**
 * Test achieved bandwidth various memcpy operations
 * @param size the size in bytes of the memcpy
 **/
void transfer(size_t size)
{
    printf("Size: %11zu [bytes]\n", size);

    float t_ms;
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    char* data_h1 = (char*) malloc(size);
    char* data_h2;
    cudaMallocHost(&data_h2, size); CUERR;
    char* data_d1;
    cudaMalloc(&data_d1,size); CUERR
    char* data_d2;
    cudaMalloc(&data_d2,size); CUERR

    cudaMemcpy(data_h2,data_h1,size,cudaMemcpyHostToHost); // Warm up the pages

    cudaEventRecord(start);
    cudaMemcpy(data_h2,data_h1,size,cudaMemcpyHostToHost);
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&t_ms,start,stop);
    printf("H2H:          Time = %11.3E [ms], Bandwidth = %11.3E [GB/s]\n", t_ms, 1.0E-6 * size / t_ms);

    cudaEventRecord(start);
    cudaMemcpy(data_d1,data_h1,size,cudaMemcpyHostToDevice);
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&t_ms,start,stop);
    printf("H2D Unpinned: Time = %11.3E [ms], Bandwidth = %11.3E [GB/s]\n", t_ms, 1.0E-6 * size / t_ms);

    cudaEventRecord(start);
    cudaMemcpy(data_d1,data_h2,size,cudaMemcpyHostToDevice);
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&t_ms,start,stop);
    printf("H2D Pinned:   Time = %11.3E [ms], Bandwidth = %11.3E [GB/s]\n", t_ms, 1.0E-6 * size / t_ms);

    cudaEventRecord(start);
    cudaMemcpy(data_h1,data_d1,size,cudaMemcpyDeviceToHost);
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&t_ms,start,stop);
    printf("D2H Unpinned: Time = %11.3E [ms], Bandwidth = %11.3E [GB/s]\n", t_ms, 1.0E-6 * size / t_ms);

    cudaEventRecord(start);
    cudaMemcpy(data_h2,data_d1,size,cudaMemcpyDeviceToHost);
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&t_ms,start,stop);
    printf("D2H Pinned:   Time = %11.3E [ms], Bandwidth = %11.3E [GB/s]\n", t_ms, 1.0E-6 * size / t_ms);

    cudaEventRecord(start);
    cudaMemcpy(data_d2,data_d1,size,cudaMemcpyDeviceToDevice);
    cudaEventRecord(stop);
    cudaEventSynchronize(stop);
    cudaEventElapsedTime(&t_ms,start,stop);
    printf("D2D:          Time = %11.3E [ms], Bandwidth = %11.3E [GB/s]\n", t_ms, 1.0E-6 * size / t_ms);

    cudaFree(data_d1); CUERR;
    cudaFreeHost(data_h2); CUERR;
    free(data_h1);

    cudaEventDestroy(start);
    cudaEventDestroy(stop);
}

// => Main Program <= //

int main()
{
    for (int k = 0; k < 31; k++) {
        transfer(1 << k);
    }
}
