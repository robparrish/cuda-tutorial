#include <cstdio>    
#include <stdexcept>

// => CUDA Error Checking Macro <= //

/**
 * Place a "CUERR;" statement after each CUDA API call to check CUDA error
 * state during debugging
 **/
#define CUERR { \
 cudaError_t e=cudaGetLastError(); \
 if(e!=cudaSuccess) { \
   printf("Cuda failure %s:%d: '%s'\n",__FILE__,__LINE__,cudaGetErrorString(e)); \
   exit(0); \
 } \
}

// => Load/Store Kernels <= //

/**
 * Load from b and store in a at address corresponding to absolute thread index + s
 * @param a the store target
 * @param b the load target
 * @param s the offset in addresses
 **/
template <typename T>
__global__ void offset(T* a, const T* b, int s)
{
    int i = blockDim.x * blockIdx.x + threadIdx.x + s;
    a[i] = b[i];
}

/**
 * Load from b and store in a at address corresponding to absolute thread index * s
 * @param a the store target
 * @param b the load target
 * @param s the offset in addresses
 **/
template <typename T>
__global__ void stride(T* a, const T* b, int s)
{
    int i = (blockDim.x * blockIdx.x + threadIdx.x) * s;
    a[i] = b[i];
}

// => Test Function <= //

/**
 * Test offset and stride load/store kernels
 * @param size_mb the number of elements to load/store in millions
 **/
template <typename T>
void test(int size_mb)
{
    // 1D grid of 1D blocks of size 64
    int blockDim = 64;
    int gridDim = size_mb*1024*1024/blockDim;

    // Device arrays (33x guarantees enough room for stride experiment)
    T* d1;
    T* d2;
    cudaMalloc(&d1, size_mb*1024*1024*33*sizeof(T)); CUERR;
    cudaMalloc(&d2, size_mb*1024*1024*33*sizeof(T)); CUERR;

    // Event timers
    float t_ms;
    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    // Offset experiment
    printf("Offset:\n");
    offset<T><<<gridDim,blockDim,0>>>(d1, d2, 0); // Warm-up
    for (int i = 0; i <= 32; i++) {
        cudaEventRecord(start);
        offset<T><<<gridDim,blockDim,0>>>(d1, d2, i);
        cudaEventRecord(stop);
        cudaEventSynchronize(stop);
        cudaEventElapsedTime(&t_ms,start,stop);
        printf("%2d: %11.3E\n", i, 2 * size_mb * sizeof(T) / t_ms);
    }

    // Stride experiment
    printf("Stride:\n");
    stride<T><<<gridDim,blockDim,0>>>(d1, d2, 1); // Warm-up
    for (int i = 1; i <= 32; i++) {
        cudaEventRecord(start);
        stride<T><<<gridDim,blockDim,0>>>(d1, d2, i);
        cudaEventRecord(stop);
        cudaEventSynchronize(stop);
        cudaEventElapsedTime(&t_ms,start,stop);
        printf("%2d: %11.3E\n", i, 2 * size_mb * sizeof(T) / t_ms);
    }
    
    // Free event timers
    cudaEventDestroy(start);
    cudaEventDestroy(stop);

    // Free device memory
    cudaFree(d1); CUERR;
    cudaFree(d2); CUERR;
}

// => Main Subroutine <= //

int main()
{
    // Try each experiment with 1 million elements
    printf("char\n");
    test<char>(1);    
    printf("float\n");
    test<float>(1);    
    printf("double\n");
    test<double>(1);    
    printf("float2\n");
    test<float2>(1);    
    printf("double2\n");
    test<double2>(1);    
    printf("float3\n");
    test<float3>(1);    
    printf("double3\n");
    test<double3>(1);    
    printf("float4\n");
    test<float4>(1);    
    printf("double4\n");
    test<double4>(1);    
}
